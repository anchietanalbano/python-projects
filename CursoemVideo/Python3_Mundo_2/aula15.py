# Conceito de while
# loop infinitos e ponto de paradas

# n = s = 0
# while True:
#     n = int(input("Digite um valor: "))
#     if n == 999:
#          break # quebra o loop se a condição for verdadeira
#     s += n
#print("A soma dos valores é {}.".format(s))

# INTERPOLACAO DE STRINGS
#print(f"A soma dos valores é {s}.") # PYTHON 3.6+
nome = 'José'
salario = 998.3
print(f"O {nome} tem {33} anos de idade e ganha R${salario:.2f}.") # também é possível ajusta as casas decimais
