"""
    Estrutura de repetição while e sua definição
"""
# Se não souber o limite do contador usa o while
# ex.:
# c = 1
# while c < 10:
#     print(c)
#     c+=1
# print("Fim")

n = -1
par = impar = 0
while n != 0:
    n = int(input('Digite um valor: '))
    if n != 0:
        if n % 2 ==0:
            par += 1
        else:
            impar+= 1
print("Numero pares {} e numeros impares {}.".format(par, impar))
print('Fim')