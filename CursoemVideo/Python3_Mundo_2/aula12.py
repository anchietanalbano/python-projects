# Condições Aninhadas

# if condicao1:
#     bloco 1
# elif condicao2:
#     bloco_2
# elif condicao3:
#     bloco_3
# else:
#     bloco_4
#  ------------------------------------------

nome = str(input('Qual é o seu nome? '))
if nome == 'Albano':
    print('Que nome diferenciado!')
elif nome == 'Pedro' or nome == 'Maria' or nome == 'Paulo':
    print('O seu nome é bem popular no Brasil.')
else:
    print('Seu nome é bem normal.')
print('Tenha um bom dia {}.'.format(nome))
