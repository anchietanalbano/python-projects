# Laços e repetições (parte 1)

# print("laço simples")
# for c in range(0,10):
#     print(c)

# for i in range(0,8,2):
#     print(i)

# i = int(input('Início: '))
# f = int(input('Fim: '))
# p = int(input('Passo: '))
#
#
# for c in range(i,f+1,p):
#     print(c)

s = 0
for i in range(0, 5):
    n = int(input('Qual valor: '))
    s += n

print('Somatório do valores foi {}'.format(s))
