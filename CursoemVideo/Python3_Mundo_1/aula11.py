# Trabalhando com cores no python
#   Style, text e back
# \033[0;   33;    44 m
# A variação desses codigos permitem muitas opções de saidas formatadas

print('\033[0;30;41mteste\033[m')
print('\033[4;33;44mteste\033[m')
print('\033[1;35;43mteste\033[m')
print('\033[30;42mteste\033[m')
print('\033[mteste\033[m')
print('\033[7;30mteste\033[m')

# Também é possivel fazer uma lista com as cores

cores = {'azul': '\033[34m',
         'amarelo': '\033[33m',
         'vermelho': '\033[31m',
         'verde':' \033[32m',
         'azulclaro':'\033[34m'}
print(cores['azul'],'#=#=#')
print(cores['amarelo'],'#=#=#')
print(cores['vermelho'],'#=#=#')
print(cores['verde'],'#=#=#')
print(cores['azulclaro'],'#=#=#')
