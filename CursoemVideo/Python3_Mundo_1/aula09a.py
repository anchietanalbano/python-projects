# Trabalhando com cadeia de caracteres

frase = 'Curso em Video Python'
print(frase)

print(frase[3:13])
print(frase[:13])
print(frase[1:15:2])
print(frase[1::2])
print(frase[::2])

print(frase.count('o'))
print(frase.count('O'))
print(frase.upper().count('O')) # colocar as letras e maiuscula e depois conta
print(len(frase)) # tamanho
print(frase.replace('Python','Android'))

# Uma string é imutavel até a mesma receber outra atribuição
# frase = frase.replace('Video', 'Audio Visual')
# print(frase)

print('Curso' in frase)
print(frase.lower().find('video'))

split = frase.split()
print(split[2][3]) # mostra a palavra e a posição da letra

# # print de texto longo
# print("""Bom dia Avanters, estamos finalizando o prazo para
# emissão dos certificados e acesso a plataforma.
# Manteremos o acesso de vocês, até a data de hoje.
# Não deixem de extrair seu certificado.
# Muito obrigado pela caminhada conjunta.""")
