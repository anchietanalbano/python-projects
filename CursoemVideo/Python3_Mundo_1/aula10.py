# condições

# ex_1.:
# nome = str(input('Qual o seu nome? '))
# if nome == 'Jose':
#     print('E ai! Tudo na boa {}.'.format(nome))
# else:
#     print('Bom dia, {}'.format(nome))
#
# print('Fim!')

# ex_2.:

n1 = float(input('Digite a primeira nota : '))
n2 = float(input('Digite a segunda nota : '))
m = (n1+n2)/2
print('A sua média foi de {:.2f}'.format(m))
# condição composta
# if m >= 6.0:
#     print('Média boa, passou!')
# else:
#     print('Média ruim, não passou!')

# condição simples
print('Muito bem, passou!' if m>=6.0 else 'Não passou!')
