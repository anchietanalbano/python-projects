n1 = int(input('um valor: '))
n2 = int(input('outro valor: '))
s = n1  + n2
m = n1 * n2
d = n1/n2
di = n1 // n2
e = n1**n2

# ao final do print, para evitar quebra de linha coloca-se  (... end=' *alguma coisa ou espaço* ')
# e para formata a saida usa-se {:alguma coisa}, como abaixo
print('A soma é {}, a multiplicação é {} e a divisão é {:.2f}'.format(s,m,d))
print('Divisão inteira {} e potencia {}'.format(di, e))