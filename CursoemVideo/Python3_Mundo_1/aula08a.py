# Compreendendo import e modulos python
# import math 1° forma de importa

# python.org - verificar documentação para importa mais pacotes.
#
# from math import sqrt
#
# num = int(input('Digite um numero: '))
# raiz = sqrt(num)
# # math.sqrt - raiz quadrada
# # math.ceil - arredonda para cima
# # math.floor - arredonda para baixo
# print('A raiz de {} é igual a {:.2f}.'.format(num,raiz))

# import random
# num = random.randint(1,10)
# print(num)

