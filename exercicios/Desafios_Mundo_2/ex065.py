"""
    Crie um programa que leia vários números inteiros pelo teclado.
    No final de execução, mostre a média entre todos os valores e qual foi
    maior e o menor. O programa deve perguntar ao usuário se ele quer ou não
    continuar a digitar valores.
"""

# Minha versão
# n = int(input("Digite um valor: "))
# p = str(input("Quer continuar? [S/N] ")).upper()
# maior = menor = n
# cont = 1
# m = 0
# while p != "N":
#     if n > maior:
#         maior = n
#     if n < menor:
#         menor = n
#     m += n
#     cont += 1
#
#     n = int(input("Digite um valor: "))
#     p = str(input("Quer continuar? [S/N] ")).upper()
#
#
# print("O programa terminou! Você digitou {} números e a media entre os valores foi {:.2f}.".format(cont,m/cont))
# print("A maior valor é {} e o menor valor {}.".format(maior, menor))

# Versão guanabara

resp = 'S'

soma = count = media = maior = menor  = 0

while resp in 'Ss':
    num = int(input("Digite um número: "))
    soma += num
    count += 1
    if count == 1:
        maior = menor = num
    else:
        if num > maior:
            maior = num
        if num < menor:
            menor = num
    resp = str(input("Quer continuar? [S/N]")).upper()
media = soma / count

print("Você digitou {} numeros e a média foi {}".format(count, media))
print("O maior valor foi {} e o menor foi {}".format(maior, menor))
