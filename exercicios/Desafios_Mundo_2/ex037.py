""" Escreva um programa que leia um número inteiro qualquer e peça
    para o usuário escolher qual será a base para conversão.
    1 binário
    2 ectal
    3 hexadecimal
    """

num = int(input('Digite um número: '))

op = int(input("""Escolha uma das bases para conversão: 
[1] converter para BINÁRIO
[2] converter para OCTAL
[3] converter para HEXADECIMAL
Sua opção: """))

if op == 1:
    print('O número {} convertido para o BINÁRIO é igual a {}'.format(num, bin(num)[2:]))
elif op == 2:
    print('O número {} convertido para o OCTAL é igual a {}'.format(num, oct(num)[2:]))
elif op == 3:
    print('O número {} convertido para o HEXADECIMAL é igual a {}'.format(num, hex(num)[2:]))
else:
    print('Opção inválida. Tente novamente.')