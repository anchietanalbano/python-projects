"""
Faça um programa que leia um NUMERO INTEIRO e diga se ele é ou não um número PRIMO.
"""

num = int(input('Digite o valor: '))
c = 0

# for i in range(1, num + 1):
#     if num % i == 0:
#         c += 1
# if c == 2:
#     print("O número {} é primo.".format(num))
# else:
#     print("O número {} NÃO é primo.".format(num))

for i in range(1, num + 1):
    if num % i == 0:
        print('\033[33m', end='')
        c += 1
    else:
        print('\033[31m', end='')
    print('{} '.format(i), end='')
if c == 2:
    print("\n\033[mO número {} é primo.".format(num))
else:
    print("\n\033[mO número {} NÃO é primo.".format(num))
