""" Crie um programa que faça o computador jogar JOKENPÔ com você.
    Pedra, Papel ou Tesoura.
"""

from random import randint
from time import sleep
itens = ('Pedra', 'Papel','Tesoura')
esc = randint(0,2)

print("""Suas opções:
[0] PEDRA
[1] PAPEL
[2] TESOURA""")
jogad = int(input('Qual é a sua jogada? '))

print('JO')
sleep(1)
print('KEN')
sleep(1)
print('PO!!!')
sleep(1)


print('=='*16)
print('O computador jogou {}'.format(itens[esc]))
print('Você jogou {}'.format(itens[jogad]))
print('=='*16)

if esc == 0:
    if jogad == 0:
        print('EMPATE')
    elif jogad == 1:
        print('VOCÊ VENCE')
    elif jogad == 2:
        print('COMPUTADOR VENCE')
    else:
        print('JOGADA INVÁLIDA!')
elif esc == 1:
    if jogad == 0:
        print('COMPUTADOR VENCE')
    elif jogad == 1:
        print('EMPATE')
    elif jogad == 2:
        print('VOCÊ VENCE')
    else:
        print('JOGADA INVÁLIDA!')
elif esc == 2:
    if jogad == 0:
        print('VOCÊ VENCE')
    elif jogad == 1:
        print('COMPUTADOR VENCE')
    elif jogad == 2:
        print('EMPATE')
    else:
        print('JOGADA INVÁLIDA!')