"""
    Escreva um programa que leia um número N inteiro
    qualquer e mostra na tela os N primeiros elementos
    de uma Sequencia de Fibonacci
    Ex.: 0-1-1-2-3-5-8
"""

print("-"*40)
print("Sequencia de Fibonacci!")
print("-"*40)

n = int(input("Quantos termos você quer mostrar? "))
t1 = 0
t2 = 1
cont = 3
print("~"*40)
print("{} -> {}".format(t1,t2), end="")
while cont <= n:
    t3 = t2 + t1
    print(" -> {}".format(t3), end="")
    t1 = t2
    t2 = t3
    cont += 1

print(" -> FIM", end="")