"""
Crie um programa que leia uma FRASE qualquer e diga se ela é
um PALÍNDRO, desconsiderando os espaços.
Ex:
APOS A SOPA
A SACADA DA CASA
A TORRE DA DERROTA
O LOBO AMA O BOLO
ANOTARAM A DATA DA MARATONA
"""

frase = input(str('Digite alguma frase: ')).strip().replace(" ", "").upper()

# palavras  = frase.split()
#  'algumacoisa'.join(palavras) = resultado exemplos '*'.join(palavras) = joao*pessoa...

#    do incio ->  <- até o fim de traz para frente
palindr = frase[::-1]

"""for i in range(len(frase)-1, -1, -1):
    palindr += frase[i]
"""

if frase == palindr:
    print("A frase é um palíndro.")
else:
    print("A frase não é um palíndro.")