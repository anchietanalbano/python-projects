""" Faça um programa que leia o ano de nascimento de um jovem e informe,
    de acordo com sua idade:
    - Se ele ainda vai se alistar ao serviço militar.
    - Se é a hora de se alistar.
    - Se já passou do tempo do alistamento.
    Seu programa também deverá mostrar o tempo que falta ou que passou do prazo.
"""

from datetime import date

ano = int(input('Informe em qual ano você nasceu: '))

idade = abs(date.today().year - ano)

if idade == 18:
    print('Você tem {} anos. Está apto para o alistamento IMEDIATO.'.format(idade))
elif idade < 18:
    print(
        'Você tem {} anos. Não está apto para o alistamento, falta {} anos para está apto ao alistamento.'.format(idade,
                                                                                                                  18 - idade))
elif idade >= 18 and idade <= 45:
    print(
        'Você tem {} anos. Ainda está apto para o serviço, porém já passou {} anos em que já devia ter se alistado.'.format(
            idade, idade - 18))
else:
    print('Você tem {} anos. Limite de idade ultrapassado, não está apto para o alistamento militar!'.format(idade))
