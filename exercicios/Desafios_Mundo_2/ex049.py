"""
Refaça o DESAFIO 009, mostrando a tabuada de um número
que o usuário escolher, só que agora ultilizando um LAÇO FOR.
"""
# Desafio 09
# Faça um programa que leia um número inteiro qualquer e mostre na tela a sua tabuada.

n = int(input('Digite um numero para ver sua tabuada: '))
print('-' * 12)
for i in range(1, 11):
    print('{} x {:2} = {}'.format(n, i, n * i))
print('-' * 12)
