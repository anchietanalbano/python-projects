"""
Desenvolva um programa que leia SEIS NÚMEROS INTEIROS e mostre a soma
apenas daqueles que forem PARES. Se o valor for IMPAR, desconsidere-o.
"""
s = 0
for u in range(1, 7):
    n = int(input("Valor {}: ".format(u)))
    if n % 2 == 0:
        s += n

print("A soma dos pares é {}".format(s))
