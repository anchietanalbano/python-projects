"""
Desenvolva um programa que leia o PRIMEIRO TERMO
e a RAZÃO de uma PA. No final, mostre os 10
primeiros termos dessa progressão.
"""
pt = int(input("Primero termo: "))
r = int(input("Razão: "))


for it in range(1,11):
    termo = pt + (it -1) * r
    print('O {}° termo é: {} '.format(it, termo))

print("Fim")
