"""
Crie um programa que leia o ANO DE NASCIMENTO de SETE PESSOAS.
No final mostre quantas pessoas ainda não
atingiram a maioridade e quantas já são maiores.
21...
"""

from datetime import date

M = 0
m = 0
anoAt = date.today().year

for i in range(1, 8):
    ano = int(input('Digite o ano em que a {}° pessoa nasceu:? '.format(i)))
    if anoAt - ano >= 21:
        M += 1
    else:
        m += 1

print('Tem {} pessoas maiores de idade;'.format(M))
print('Tem {} pessoas menores de idade;'.format(m))
