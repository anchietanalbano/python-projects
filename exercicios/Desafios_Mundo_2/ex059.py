"""
    Calculadora
    Crie um programa que leia dois valores e mostre um menu tela:
    [1] somar
    [2] multiplicar
    [3] maior
    [4] novos números
    [5] sair do programa
    Seu programa deverá realizar a operação solicitada em cada caso.
"""
import time

print('Calculadora.')
n1 = int(input('Digite o primeiro valor: '))
n2 = int(input('Digite o segundo valor: '))
op = 0
while (op != 5):
    op = int(input("""Escolha uma da opções da calculadora: \n[1] somar\n[2] multiplicar\n[3] maior\n[4] novos números\n[5] sair do programa
    Qual a sua escolha? """))
    if op == 1:
        print("A soma dos números é {}.".format(n1 + n2))
    elif op == 2:
        print("A multiplicação dos números é {}.".format(n1 * n2))
    elif op == 3:
        if n1 > n2:
            print("O maior número é {}.".format(n1))
        else:
            print("O maior número é {}.".format(n2))
    elif op == 4:
        n1 = int(input('Novo valor um: '))
        n2 = int(input('Novo valor dois: '))
    elif op == 5:
        print('Saindo do programa...')
        time.sleep(0.5)
    else:
        print('Opção invalida.')
print('Fim do programa!')
