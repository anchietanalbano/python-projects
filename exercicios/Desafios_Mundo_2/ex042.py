""" Refaça o DESAFIO 35 dos triângulo, acrescentando o recurso de moestrar que tipo
    de triângulo será formado:
    - Equilátero: Todos os lados iguais.
    - Isósceles: dois lados iguais
    - Escaleno: Todos os lados diferentes
    Desafio 35
    Desenvolva um programa que leia o comprimento de três retas e diga ao usuário
    se elas podem ou não formar um triângulo.
"""

r1 = float(input('Primeiro segmento: '))
r2 = float(input('Segundo segmento: '))
r3 = float(input('Terceiro segmento: '))

if r1 < r2 + r3 and r2 < r1 + r3 and r3 < r1 + r2:
    print('Os segmentos podem forma um triângulo ',end='')
    if r1 == r2 and r2 == r3:
        print('Equilátero, pois possui todos os lados iguais.')
    elif r1 == r2 or r1 == r3 or r2 == r3:
        print('Isósceles, pois possui dois lados iguais.')
    else:
        print('Escaleno, pois possui todos os lados diferentes.')
else:
    print('Os segmentos não podem forma um triângulo!')
