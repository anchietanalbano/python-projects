"""
Faça um programa que leia o PESO de CINCO PESSOAS.
No final, mostre qual foi o MAIOR peso e o MENOR
peso lidos.
"""

maior = 0
menor = 0

for i in range(1, 6):
    peso = float(input("Digite o peso da {}° pessoa: ".format(i)))
    if i == 1:
        menor = peso
        maior = peso
    else:
        if peso > maior:
            maior = peso
        if peso < menor:
            menor = peso

print("O maior peso lido é {}.km".format(maior))
print("O menor peso lido fé {}.km".format(menor))
