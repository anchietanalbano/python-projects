"""
    Crie um programa que leia o NOME e o PREÇO de vários produtos.
    O programa deverá perguntar se o USUÁRIO vai continuar. No final,
    mostre:
    A) Qual é o total gasto na compra.
    B) Quantos produtos custam mais de R$1000
    C) Qual é o nome do produto mais barato.
"""

print("-"*18)
print("LOJA SUPER BARATÃO")
print("-"*18)

total = Maior = MaiorMil = 0
barato = 1000
NomeMaisBarato = ''
while True:
    nome = str(input("Nome do produto: "))
    preco = float(input("Preço: R$ "))
    total += preco
    c = str(input("Quer continuar? [S/N]")).upper()
    if c in 'Nn':
        break
    if preco >= 1000:
        MaiorMil += 1
    if preco < barato:
        barato = preco
        NomeMaisBarato = nome

print(f'O total gasto na compra foi de R$ {total:.2f}')
print(f'Temos {MaiorMil} produtos custando mais de R$1000.00')
print(f'O produto mais barato foi {NomeMaisBarato} que custa R${barato}')