"""
    Crie um programa que leia vários números de inteiros pelo teclado.
    O programa só vai parar quando o usuário digitar o valor 999, que é a codição
    de parada. No final mostre quantos números foram digitados e qual foi a soma entre
    eles.
"""

qt = s = 0
while True:
    n = int(input('Digite um valor (999 - parar): '))
    if n == 999:
        break
    s += n
    qt += 1

print(f'O soma dos {qt} números foi de {s}.')
