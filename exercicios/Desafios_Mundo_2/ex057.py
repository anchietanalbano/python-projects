"""
    Faça um programa que leia o sexo de uma pessoa, mas só aceite
    os valores 'M' e 'F'. Caso esteja errado, peça a digitação novamente
    até ter um valor correto.
"""

# meu jeito
# sexo = ''
# c = -1
# while c < 0:
#     sexo = str(input('Digite o seu sexo - [M/F]: ')).strip().upper()[0]
#     if sexo == 'M' or sexo == 'F':
#         print('Sexo {} valido'.format(sexo))
#         c += 1
#     else:
#         print('Sexo invalido digite novamente.')
# print('Fim')

# Jeito Guanabara

sexo = str(input('Informe seu sexo: [M/F] ')).strip().upper()[0]
while sexo not in 'MmFf':
    sexo = str(input('Dados inválidos. Por favor, informe seu sexo: ')).strip().upper()[0]

print('Sexo {} registrado com sucesso.'.format(sexo))