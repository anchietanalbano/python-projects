"""
    Leia um número qualquer e
    mostre o seu FATORIAL
    ex.:
    5! = 120
"""
# por modulos
# from math import factorial
# n = int(input('Digite um valor para calcular seu Fatorial: '))
# print("O fatórial de {}! é {}.".format(n,factorial(n)))

# por repetição while
# c = n = int(input('Digite um valor para calcular seu Fatorial: '))
# f=1
# print("Calculando {}! = ".format(n), end='')
# while c > 0:
#     print('{}'.format(c),'x ' if c > 1 else '=', end='')
#     f *=c
#     c -= 1
# print(" {}.".format(f))

# com repetição for
n = int(input('Digite um valor para calcular seu Fatorial: '))

print("Calculando {}! = ".format(n), end='')
f=1
for i in range(n,0,-1):
    print('{}'.format(i), 'x ' if i > 1 else '= ', end='')
    f *= i

print("{}".format(f))

