""" A Confederação Nacional de Natação precisa de um programa
    que leia o ano de nascimento de um atleta e mostre sua categoria,
    de acordo com a idade:
    - Até 9 anos: MIRIM
    - Até 14 anos: INFANTIL
    - Até 19 anos: SÊNIOR
    - Acima: MASTER
"""

from datetime import date

ano = int(input('Informe seu ano de nascimento: '))

idade = date.today().year - ano

if idade <= 9:
    print('Você tem {} anos.\nCategoria indicada: MIRIM.'.format(idade))
elif idade <= 14:
    print('Você tem {} anos.\nCategoria indicada: INFANTIL.'.format(idade))
elif idade <= 19:
    print('Você tem {} anos.\nCategoria indicada: JUNIOR.'.format(idade))
elif idade <= 25:
    print('Você tem {} anos.\nCategoria indicada: SÊNIOR.'.format(idade))
else:
    print('Você tem {} anos.\nCategoria indicada: MASTER.'.format(idade))