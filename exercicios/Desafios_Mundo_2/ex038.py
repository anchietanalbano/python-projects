""" Escreva um program que leia dois números inteiros e compare-os,
    mostrando na tela uma mensagem:
    - O primeiro valor é maior
    - O segundo valor é maior
    - Não existe valor maior, os dois são iguais.
"""
n1 = int(input('Primeiro valor: '))
n2 = int(input('Segundo valor: '))

if n1 == n2:
    print('Não existe valor maior, os dois são iguais.')
elif n1 > n2:
    print('O primeiro valor {} é o maior.'.format(n1))
else:
    print('O segundo valor {} é o maior.'.format(n2))