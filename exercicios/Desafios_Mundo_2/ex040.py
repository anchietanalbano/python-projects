""" Crie  um programa que leia duas notas de um aluno e calcule sua média,
    mostrando uma mensagem no final, de acordo com a média atingida:
    - Média abaixo do 5.0: REPROVADO
    - Média entre 5.0 e 6.9: RECUPERAÇÃO
    - Média entre 7.0 ou superior: APROVADO
"""

n1 = float(input('Primeira nota: '))
n2 = float(input('Segunda nota: '))

media = (n1 + n2) / 2

# if media < 5.0:
if media < 5:
    print('Média {:.1f}, está REPROVADO'.format(media).upper())
elif 7 > media >=5:
    print('Média {:.1f}, precisa fazer RECUPERAÇÃO'.format(media))
else:
    print('Média {:.1f}, APROVADO'.format(media).upper())
