"""
    Crie um programa que simule o funcionamento de um caixa eletrônico.
    No início, pergunte ao usuário qual será o valor a ser sacado [número inteiro] e o
    programa vai informar quantas cédulas de cada valor serão entregues.

    OBS: Considere que o caixa possui cédulas de R$50, R$20, R$10 e R$1
"""
print("=" * 11)
print(" BANCO CEV ")
print("=" * 11)

valor = float(input('Qual valor você quer sacar? R$ '))

ciquenta = vinte = dez = um = 0

while True:
    valor -= 50
    if valor <= 0:
        valor += 50
        break
    ciquenta += 1

while True:
    valor -= 20
    if valor <= 0:
        valor += 20
        break
    vinte += 1

while True:
    valor -= 10
    if valor <= 0:
        valor += 10
        break
    dez += 1

while True:
    valor -= 1
    if valor <= 0:
        um += 1
        break
    um += 1

print(f'Total de {ciquenta} cédulas de R$50')
print(f'Total de {vinte} cédulas de R$20')
print(f'Total de {dez} cédulas de R$10')
print(f'Total de {um} cédulas de R$1')
