""" Escreva um programa para aprovar o empréstimo bancário para compra de uma casa.
    O programa vai perguntar o valor da casa, o salário do comprador e em quantos anos
    ele vai pagar.
    Calcule o valor da prestação mensal, sabendo que ela não pode exceder 30% do salário
    ou então o empréstimo será negado.
"""
vCasa = float(input('Qual o valor da casa? R$'))
salCom = float(input('Qual o valor do seu salário? R$'))
qt_anos = int(input('Qual a quantidade de anos em que pretende pagar? '))

qt_meses = qt_anos*12
vPres = vCasa/qt_meses

if vPres > (salCom*30/100):
    print('Infelizmente, não é possível um empréstimo nas suas condições financeiras atuais!\nA prestação vale R${:.2f}, excede 30% do seu salário,\ntente novamente aumentando o prazo ou escolhendo outra casa.\nEmpréstimo NEGADO!'.format(vPres))
else:
    print('O valor da prestação para compra do imóvel valendo R${:.2f}, foi estimado em R${:.2f} por mês.'.format(vCasa,vPres)+'\nEmpréstimo APROVADO')

