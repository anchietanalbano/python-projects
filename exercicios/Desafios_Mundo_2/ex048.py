"""
Faça um programa que calcule a soma entre todos os NÚMEROS IMPARES e
que são MÚLTIPLOS DE TRÊS e que se encotram no intervalo de 1 até 500.
"""

s = 0
cont = 0
for i in range(1, 501):
    if i % 2 != 0 and i % 3 == 0:
        cont += 1
        s += i
print("A soma de todos o número {} de 1 a 500 multiplos de 3 é {}".format(cont, s))
