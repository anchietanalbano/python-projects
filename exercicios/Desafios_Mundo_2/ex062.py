"""
    Melhore o DESAFIO 061, perguntando para o usuário se ele quer
    mostrar mais alguns termos. O programa encerra quando ele disser
    que quer mostrar 0 termos.
"""

pt = int(input("Primeiro termo: "))
r = int(input("Razão: "))
term = pt
cont = 1
mais = 10
total = 0
while mais != 0:
    total += mais
    while cont <= total:
        print("{} -> ".format(term), end="")
        term += r
        cont += 1
    print("PAUSA")
    mais = int(input('Quantos termos a mais você quer mostrar? '))
print('Progressão finalizada com {} termos.'.format(total))
