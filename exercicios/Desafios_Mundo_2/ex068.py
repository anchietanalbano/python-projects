"""
    Faça um programa que jogue par ou impar com o computador.
    O jogo só será interrompido quando o jogador PERDER, mostrando
    o total de vitórias consecutivas que ele conquistou no final no jogo
"""

from random import randint

print("=-"*15)
print("VAMOS JOGAR PAR OU ÍMPAR")
print("=-"*15)

n = s = 0
while True:
    cop = randint(0,15)
    n = int(input("Diga um valor: "))
    J = str(input("Par ou Ímpar? [P/I] ")).upper()
    print("_"*20)
    if n+cop%2==0 and J == 'P':
        s += 1
        print(f'Você jogou {n} e o computador {cop}. Total de {n+cop} DEU PAR')
        print("_" * 20)
        print('Você VENCEU! \nVamos jogar novamente...')
        print("=-" * 15)
    if n+cop%2!=0 and J == 'I':
        s+= 1
        print(f'Você jogou {n} e o computador {cop}. Total de {n+cop} DEU ÍMPAR')
        print("_" * 20)
        print('Você VENCEU! \nVamos jogar novamente...')
        print("=-" * 15)
    else:
        break

print('Você PERDEU!')
print("=-"*15)
print(f'GAME OVER! Você venceu {s} vezes.')