"""
    Melhore o jogo do DESAFIO 028 onde o computador vai "pensar" em
    número entre 0 e 10. Só que agora o jogador vai tentar adivinhar até
    acertar, mostrando quantos palpites foram necessários para vencer.
    # desafio 28
    # Escreva um programa que faça o computador 'pensar' em um número inteiro entre 0 a 5
    # e peça para o usuario tentar descobir qual foi o número escolhido pelo computador.
    # O programa deve escrever na tela caso o usuario acerte ou erre
"""

from random import randint
from time import sleep

# Minha tentativa
# n = randint(1, 5)
# op = 0
# while(n != op):
#     print('O computador escolheu um número...')
#     op = int(input('Tente advinhar? '))
#     sleep(1)
#     if n == op:
#         print('Você acertou, parabéns!')
#     else:
#         print('Você errou, tente outra vez!')

computador = randint(0,10)
print('O computador escolheu um número...')
acertou=False
palpites = 0
while not acertou:
    jogador = int(input("Qual o seu palpite? "))
    palpites += 1
    if jogador == computador:
        acertou = True
    else:
        if jogador < computador:
            print('Mais... Tente outra vez.')
        else:
            print('Menos... Tente outra vez.')

print('Acertou com {} tentativas. Muito bem!'.format(palpites))
