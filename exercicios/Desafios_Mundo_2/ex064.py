"""
    Crie um programa que leia vários números inteiros pelo teclado
    O programa só vai parar quando o usuário digitar o valor 999, que é a
    condição de parada. No final, mostra quantos números foram digitados
    e qual foi a soma entre eles.
"""

valor = int(input("Digite um número [999 para parar]: "))

cont = 0
soma = valor
while valor != 999:
    cont += 1
    soma += valor
    valor = int(input("Digite um número [999 para parar]: "))

print("Você digitou {} números. E soma do números é {}.".format(cont, soma))
