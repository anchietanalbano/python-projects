""" Elabore um programa que calcule o valor a ser pago por um produto,
    considerando o seu 'preço normal' e 'condição de pagamento':
    - à vista dinheiro/pix: 10% off
    - à vista no cartão: 5% off
    - em até 2x no cartão: normal
    - 3x ou mais no cartão: 20% on
"""

print('=' * 10 + ' LOJAS ALBAN ' + '=' * 10)
preco = float(input('Preço das compras: R$'))

print('-' * 5 + 'Formas de pagamento:' + '-' * 5 +
      """\n
         1 : À vista dinheiro/pix.
         2 : À vista no cartão.
         3 : Em até 2x no cartão.
         4 : 3x ou mais no cartão.""")
op = int(input("Escolha uma opção: "))

# match (op):
#     case (1):
#         preco = preco - (preco * 10 / 100)
#         print('O valor a pagar será de R${:.2f}.'.format(preco))
#     case (2):
#         preco = preco - (preco * 5 / 100)
#         print('O valor a pagar será de R${:.2f}.'.format(preco))
#     case (3):
#         preco = preco / 2
#         print('O valor a pagar será 2x de R${:.2f}.'.format(preco))
#     case (4):
#         preco = preco + (preco * 20 / 100)
#         print('O valor a pagar será de R${:.2f}.'.format(preco))

if op == 1:
    print('Sua compra de R${:.2f} à vista, será de R${:.2f}.'.format(preco,
                                                                     (preco - (preco * 10 / 100))))
elif op == 2:
    print('Sua compra de R${:.2f}, à vista no cartão será de R${:.2f}.'.format(preco, (
            preco - (preco * 5 / 100))))
elif op == 3:
    print('Sua compra de R${:.2f}, em 2x no cartão, será de R${:.2f}. SEM JUROS.'.format(preco, (preco / 2)))
elif op == 4:
    vezes = int(input('Quantas parcelas? '))
    coJuros = preco + (preco * 20 / 100)
    print(
        'Sua compra será parcelada em {}x, de R${} COM JUROS.\n Sua compra de R${} vai custar R${:.2f} no final.'.format(
            vezes, (coJuros / vezes), preco, coJuros))
else:
    print('\033[31mOpção inválida!.')
