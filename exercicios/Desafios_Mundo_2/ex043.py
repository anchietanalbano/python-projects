""" Desenvolva uma lógica que leia o peso e a altura de uma pessoa,
    calcule seu IMC e mostre se status, de acordo com a tabela abaixo:
    - Abaixo de 18.5: Abaixo do Peso
    - Entre 18.5 a 25: Peso ideal
    - 25 até 30: Sobrepeso
    - 30 até 40: Obesidade
    - Acima de 40: Obesidade mórbida
"""
p = float(input('Informe seu peso (km): '))
a = float(input('Informe sua altura (m): '))

imc = p/(a**2)
print('O seu IMC é {:.2f}.'.format(imc))
if imc < 18.5:
    print('Você está abaixo do peso!')
elif 18.5 <= imc < 25:
    print('Você está no peso ideial.')
elif 25 <= imc < 30:
    print('Você está com sobrepeso!')
elif 30 <= imc < 40:
    print('Você está com obesidade! Cuidado!')
else:
    print('Você tá fudido cara, está com obesidade mórbida!')

