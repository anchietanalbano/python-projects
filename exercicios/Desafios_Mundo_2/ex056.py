"""
Desenvolva um programa que leia o NOME, IDADE e SEXO de 4 PESSOAS.
No final do programa, mostre:

- A MÉDIA DE IDADE do grupo.
- Qual é o nome do homem MAIS VELHO.
- Quantas mulheres têm MENOS DE 20 anos.
"""

# media = 0
# mulher = 0
# mIdade = 0
# homem = ''
#
# for i in range(0, 4):
#     print("--" * 20)
#     nome = str(input('Digite o nome da pessoa: '))
#     idade = int(input('Digite o idade da pessoa: '))
#     sexo = str(input('Digite o sexo da pessoa - [M/F]: ')).upper()
#
#     media += idade
#
#     if sexo == "m" and idade > mIdade:
#         homem = nome
#         mIdade = idade
#     if sexo == "f" and idade < 20:
#         mulher += 1
#
# print("A media de idade é {:.1f}.".format(media/4))
# print("Nome do homem mais velho é {}.".format(homem))
# print("Mulheres menores de idade: {}".format(mulher))

idades = 0
maiorhomem = 0
nomeVelho = ''
totmulher = 0

for d in range(1, 5):
    print('------ {} PESSOA ------'.format(d))
    nome = input('Nome: '.format(d)).strip()
    idade = int(input('Idade: '.format(d)))
    sexo = input('Sexo: [M/F]'.format(d)).strip()
    idades += idade

    if d == 1 and sexo in 'Mm':
        maiorhomem = idade
        nomeVelho = nome
    if sexo in 'Mm' and idade > maiorhomem:
        maiorhomem = idade
        nomeVelho = nome
    if sexo in 'Ff' and idade < 20:
        totmulher+=1

media = idades / 4

print("A media de idade é {:.1f}.".format(media))
print("O homem mais velho tem {} anos e se chama {}.".format(maiorhomem,nomeVelho))
print("Tem {} mulheres menores de 20 anos".format(totmulher))