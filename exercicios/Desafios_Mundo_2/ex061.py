"""
    Refaça o DESAFIO 051, lendo o primeiro termo e a razão
    de uma PA, mostrando os 10 primeiros termos da progressão
    usando a estrutura while
"""

pt = int(input("Primeiro termo: "))
r = int(input("Razão: "))
term = pt
cont = 1

while cont <= 10:
    print("{} -> ".format(term), end="")
    term += r
    cont += 1
print('Fim.')
