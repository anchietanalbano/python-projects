"""
Crie um programa que mostre na tela todos os pares que estão
no intervalo entre 1 e 50.
"""

print("O pares no intervalo de 1 a 50 são: ")
# for i in range(1, 51):
#     print(".",end='')
#     if(i%2==0):
#         print(i,end=" ")
# print("\nFIM")

for i in range(2, 51, 2):
    print(".",end='')
    print(i,end=" ")
print("FIM")