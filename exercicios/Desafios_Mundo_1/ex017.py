# desafio 17
# Faça um programa que leia o comprimento do cateto oposto e do cateto adjacente de um triângulo retângulo,
# calcule e mostre o comprimento da hipotenusa
import math

co = float(input("O valor do cateto oposto: "))
ca = float(input("O valor do cateto adjacente: "))
# op = math.sqrt( (pow(co, 2)) + (pow(ca, 2)))
# biblioteca do python que calcula o hipotenusa - math.hypoy()
op = ((co**2)+(ca**2))**(1/2)
# op = math.hypot(co, ca)
print("o valor da hipotenusa é {:.2f}".format(op))
