# desafio 06
# Crie um algoritmo que leia um número e mostre o seu dobro, triplo e raiz quadrada

num = int(input('Digite um número: '))
# print('O dobro de {} é {}, o triplo é {} e a raiz quadrada é {:.2f}'.format(num,(num*2),(num*3),(num**(1/2))))  #minha versão
# versão guanabara
print('O dobro de {} vale {}.\nO triplo de {} vale {}.\nA raiz de {} vale {:.2f}.'.format(num,(num * 2),num,(num * 3),num,(num**(1/2))))
