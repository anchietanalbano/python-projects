# Faça um programa que leia algo pelo teclado e mostre na tela o seu tipo primitivo e
# todas as informações possiveis sobre ele.

valor = input('Digite algo: ')
print('O tipo primitivo desse valor é {}'.format(type(valor)))
print('Só tem espaços? {}'.format(valor.isspace()))
print('É um número? {}'.format(valor.isnumeric()))
print('É alfabético? {}'.format(valor.isalpha()))
print('É alfanumérico? {}'.format(valor.isalnum()))
print('Está com letras maiúsculas? {}'.format(valor.isupper()))
print('Está com letras minúsculas? {}'.format(valor.islower()))
print('Está capitalizada?'.format(valor.istitle()))


print('Está na tabela ascii: {}'.format(valor.isascii()))
print('É um digito: {}'.format(valor.isdigit()))
print('É decimal: {}'.format(valor.isdecimal()))
print('É identifier: {}'.format(valor.isidentifier()))
print('É printable: {}'.format(valor.isprintable()))
print('É subclass: {}'.format(valor.__init_subclass__()))
