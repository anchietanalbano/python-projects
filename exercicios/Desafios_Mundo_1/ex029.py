# desafio 29
# Escreva um programa que leia a velocidade de carro.
# Se ele ultrapassar 80km/h, mostre uma mensagem dizendo que ele foi multado.
# A multa vai custar R$7,00 por cada Km acima do limite.

vel = float(input('Qual a velocidade do carro: '))
print('Está no conforme, tudo ok!' if vel < 80 else 'Excesso de velocidade, você foi multado no valor de R${:.2f}!'.format((vel - 80) * 7))

