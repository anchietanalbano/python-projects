# Desafio 12
# Faça um algoritmo que leia o preço de um produto e mostre seu novo preço, com 5% de desconto.

# minha versão
# valor = int( input('Valor do produto: '))
# desco = valor*(5/100)
# print('O valor do produto na liquidação é R${:.2f}'.format(valor-desco))
#
# versão guanabara

preco = float(input('Qual o preço do produto? R$'))
novo = preco - (preco * 5 / 100)
print('O produto que custava R${:.2f}, na promoção com desconto de 5% vai custar R${:.2f}.'.format(preco,novo))
