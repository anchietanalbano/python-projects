# desafio 10
# Crie um programa que leia quanto dinheiro uma pessoa tem na carteira e
# mostre quantos dolares ela pode comprar.

# minha versão
# real = int(input('Quanto tem na carteira? '))
# dolar = real/5.25
# print('Você pode comprar US${:.2f} dolares e seja feliz!'.format(dolar))
#
# versão guanabara

real = float(input('Quando dinheiro você tem na carteira? R$'))
dolar = real/5.35
print('Com R${:.2f} você pode comprar US${:.2f}'.format(real,dolar))