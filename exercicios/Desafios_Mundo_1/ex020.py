# desafio 20
# O mesmo professor do desafio anterior quer sortear a ordem de apresentação de trabalhos do alunos.
# Faça um programa que leia o nome dos quatro alunos e mostre a ordem sorteada
from random import shuffle

a1 = input('1° aluno: ')
a2 = input('2° aluno: ')
a3 = input('3° aluno: ')
a4 = input('4° aluno: ')

lista = [a1, a2, a3, a4]

# metodo shuffle - embaralhar
shuffle(lista)

print('A ordem de apresentação será {}'.format(lista))
