# desafio 30
# Crie um programa que leia número inteiro e mostre na tela se ele é PAR ou IMPAR.

n = int(input('Digite um valor: '))
print('O número é PAR!' if n % 2 == 0 else 'O número é IMPAR!')
