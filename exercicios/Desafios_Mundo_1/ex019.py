# desafio 19
# Um professor que sortear um dos seus quatro alunos para apagar o quadro. Faça um programa que
# ajude ele, lendo o nome deles e escrevendo o nome do escolhido.
import random

alu1 = input('Digite o nome do 1° aluno: ')
alu2 = input('Digite o nome do 2° aluno: ')
alu3 = input('Digite o nome do 3° aluno: ')
alu4 = input('Digite o nome do 4° aluno: ')

# incio de list
lista = [alu1, alu2, alu3, alu4]
# elemento choice = uma escolha dentro de
aluSor = random.choice(lista)

print("O aluno sorteado é {}".format(aluSor))
