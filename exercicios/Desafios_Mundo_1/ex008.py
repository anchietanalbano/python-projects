# desafio 08
# Escreva um programa que leia um valor em metros e o exiba convertido em
# centimetros e milimetros

# Minha versão
#valor = int(input('Quantos metros: '))
#
# c = valor*100
#
# print('O valor {} convertido para centimetros é {}cm e convertido para milímetros é {}mm'.format(valor,c,c*10))

# Versão Guanabara

medida = float(input('Uma medida em metros: '))
km = medida * 0.001
hm = medida* 0.01
dam = medida * 0.1
dc = medida * 10
cm = medida * 100
mm = medida * 1000
print('A medida de {}m corresponde a {}km, {}hm, {}dam, {}dc, {}cm e {}mm.'.format(medida,km,hm,dam,dc,cm,mm))
