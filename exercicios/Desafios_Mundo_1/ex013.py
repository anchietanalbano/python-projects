# desafio 13
# Faça um algoritmo que leia o sálario de um funcionario de mostre seu
# novo sálario , com um aumento de 15%.

# Minha versão
# si = int(input('Digite o sálario atual: '))
# aumS = si*(15/100)
# print('O sálario atual R${:.2f} com o aumento de 15%, será de R${:.2f}'.format(si,si+aumS))

# versão guanabara

salario = float(input('Qual é o salário do Funcionario? R$'))
novo = salario + (salario * 15 / 100)
print('Um funcionário que guanhava R${:.2f}, com 15% de aumento, passa a receber R${:.2f}'.format(salario,novo))
