# Faça um programa que leia a largura e a altura de uma parede em metros,
# calcule a sua área e a quantidade de tinta necessária para pínta-la, sabendo
# que cada litro de tinta pinta uma área de 2m²

# minha versão
# h = int(input('Altura da parede: '))
# l = int(input('Largura da parede: '))
#
# a = l*h
# pa = a/2
# print('A área da parde é {}m² e a quantidade de tinta necessária para pinta-la é {}L'.format(a,pa))

# versão guanabara

larg = float(input('Largura da parede: '))
alt = float(input('Altura da parede: '))
area = larg*alt
print('Sua parede tem a dimesão de {}x{} e sua área é de {}m².'.format(larg,alt,area))
tinta = area/2
print('Para pintar a parede, você precisará de {}l de tinta.'.format(tinta))
