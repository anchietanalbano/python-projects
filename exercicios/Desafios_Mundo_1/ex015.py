# desafio 15
# Escreva um programa que pergunte a quantidade de Km percorridos por um carro alugado e a quantidade de dias pelos quais
# foi alugado. Calcule o preço a pagar, sabendo que o carro custa R$60 por dia e R$0,15 por KM rodado.

dias = int(input('Quantos dias o carro foi usado? '))
km = int(input('Quantos km foi percorrido com o carro? '))

pago = (60 * dias) + (km * 0.15)

print("O valor a pagar por {} dias e {}km, é de R${:.2f}".format(dias, km, pago))
