# desafio 07
# Desenvolva um programa que leia as duas notas de um aluno, calcule e mostre a sua média

# Minha versão
# n1 = int(input('Primeira nota: '))
# n2 = int(input('Segunda nota: '))
# print('A média das notas é {:.2f}'.format((n1 + n2) / 2))

# Versão guanabara

n1 = float(input('Primeira nota: '))
n2 = float(input('Segunda nota: '))
média = (n1 + n2)/2
print('A média entre {:.1f} e {:.1f} é igual a {:.1f}'.format(n1,n2, média))