# Desafio 09
# Faça um programa que leia um número inteiro qualquer e mostre na tela a sua tabuada.

# Minha versão
#valor = int(input('Digite um numero: '))
#
# print('Tabuada de {}'.format(valor))
# print('1 * {} = {}'.format(valor,valor*1))
# print('2 * {} = {}'.format(valor,valor*2))
# print('3 * {} = {}'.format(valor,valor*3))
# print('4 * {} = {}'.format(valor,valor*4))
# print('5 * {} = {}'.format(valor,valor*5))
# print('6 * {} = {}'.format(valor,valor*6))
# print('7 * {} = {}'.format(valor,valor*7))
# print('8 * {} = {}'.format(valor,valor*8))
# print('9 * {} = {}'.format(valor,valor*9))
# print('10 * {} = {}'.format(valor,valor*10))

# versão guanabara
n = int(input('Digite um numero para ver sua tabuada: '))
print('-'*12)
print('{} x {:2} = {}'.format(n,1,n*1))
print('{} x {:2} = {}'.format(n,2,n*2))
print('{} x {:2} = {}'.format(n,3,n*3))
print('{} x {:2} = {}'.format(n,4,n*4))
print('{} x {:2} = {}'.format(n,5,n*5))
print('{} x {:2} = {}'.format(n,6,n*6))
print('{} x {:2} = {}'.format(n,7,n*7))
print('{} x {:2} = {}'.format(n,8,n*8))
print('{} x {:2} = {}'.format(n,9,n*9))
print('{} x {:2} = {}'.format(n,10,n*10))
print('-'*12)
