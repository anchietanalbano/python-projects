# desafio 26
# Faça um programa que leia uma frase pelo teclado e mostre:
# Quantas vezes aparece a letra "A".
# Em que posição ela aparece a primeira vez.
# Em que posição ela aparece a última vez.

frase = str(input('Digite uma frase: ')).strip()

print('A letra A aparece {} vezes.'.format(frase.lower().count('a')))
print('A letra A apareceu na posição {}'.format(frase.lower().find('a')+1))
print('A última letra A apareceu na posição {}'.format(frase.lower().rfind('a')+1))

# alguns metodoso possuem um mudança em suas funções adicionando por exemplo o right e left
# que foi o caso dessa questão usando find - rfind()