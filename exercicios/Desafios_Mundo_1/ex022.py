# desafio 22
# Crie um programa que leia o nome completo de uma pessoa e mostre:
# - O nome com todas as letras maiúsculas
# - O nome com todas minúscula
# - Quantas letras tem (sem considera espaços)
# - Quantas letras tem o primeiro nome.

nome = input('Digite o nome completo: ').strip()

print('O nome com as letras maiúsculas {}'.format(nome.upper()))
print('O nome com as letras minúsculas {}'.format(nome.lower()))
# minha versão
print('Quantas letras tem ao todo: {}'.format(len(nome.replace(' ',''))))
# versão Guanabara
print('Quantas letras tem ao todo: {}'.format(len(nome) - nome.count(' ')))
nm = nome.split()
print('Quantas letras tem o primeiro nome: {}'.format(len(nm[0])))

