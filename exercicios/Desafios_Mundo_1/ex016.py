# desafio 16
from math import trunc
# Crie um programa que leia um número Real qualquer pelo teclado e mostre na tela a sua porção inteira.
# Ex.: número 6.127 = 6

# n = float(input('Digite um número: '))
# print("O número {} tem a parte inteira {}".format(n,math.ceil(n)))

# trunc() - retira somente a parte inteira

n = float(input('Digite um número: '))
print('O número digitado foi {} e sua parte inteira é {}.'.format(n,trunc(n)))

# ao invés de importa a bibliotéca math, poderia somente transforma lo para inteiro - int(number)
