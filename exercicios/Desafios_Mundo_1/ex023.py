# desafio 23
# Faça um programa que leia um número de 0 a 9999 e mostre na tela cada um dos
# digitos separados.
# Ex.: 2834
# unidade: 4
# dezena: 3
# centena: 8
# milhar: 2

num = int(input('Informe um numero: '))

print('Analisando o número {}'.format(num))

print('Unidade: {}'.format(num % 10))
print('Dezena: {}'.format(num // 10 % 10))
print('Centena: {}'.format(num // 100 % 10))
print('Milhar: {}'.format(num // 1000 % 10))
