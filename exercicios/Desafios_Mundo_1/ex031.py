# desafio 31
# Desenvolva um programa que pergunte a distância de uma viagem em
# Calcule o preço da passagem, cobrando R$0,50 por Km para viagens de até
# 200Km e R$0,45 para viagens mais longas.

dis = float(input('Qual é a distãncia percorrida? '))
valor = (dis * 0.45) if dis >= 200 else (dis * 0.50)
print('O valor da sua passagem por percorrer {} km será de R${:.2f}.'.format(dis, valor))
