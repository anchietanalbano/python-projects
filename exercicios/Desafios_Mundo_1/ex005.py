# desafio 05
# Faça um programa que leia um numero inteiro e mostre na tela o seu sucessor e antecessor.

n = int(input('Digite um número: '))
print('O sucessor de {} é {} e o seu antecessor é {}'.format(n,n+1,n-1))
