# desafio 28
# Escreva um programa que faça o computador 'pensar' em um número inteiro entre 0 a 5
# e peça para o usuario tentar descobir qual foi o número escolhido pelo computador.
# O programa deve escrever na tela caso o usuario acerte ou erre

from random import randint
from time import sleep

n = randint(0, 5)

print('-=-' * 20, '\nO computador escolheu um número...Adivinhe qual é:\n', '-=-' * 20)
op = int(input('O que você acha? '))
print('PROCESSANDO...')
sleep(2)
print('Parabéns, acertou!!' if op == n else 'Eita, errou! O número escolhido foi {} e não {}!'.format(n, op))
