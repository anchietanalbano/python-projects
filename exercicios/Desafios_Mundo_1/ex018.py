# desafio 18
# faça um programa que leia um ângulo qualquer e mostre na tela o valor do
# seno, cosseno e tangente de ângulo

from math import sin,cos,tan,radians

angulo = float(input('Digite um angulo: '))
# Para calcular o seno, cosseno ou tangente com a biblioteca math, precisa transforma a entrada de dados em radianos
# já que os metodos dos mesmos só aceita em radiants
sen = sin(radians(angulo))
cos = cos(radians(angulo))
tan = tan(radians(angulo))
print('O angulo {} tem o SENO de {:.2f}'.format(angulo,sen))
print('O angulo {} tem o COSSENO de {:.2f}'.format(angulo,cos))
print('O angulo {} tem o TANGENTE de {:.2f}'.format(angulo,tan))
