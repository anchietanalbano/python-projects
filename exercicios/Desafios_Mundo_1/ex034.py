# desafio 34
# Escreva um programa que pergunte o salário de
# um funcionário e calcule o valor do seu aumento.
# Para salários superiores a R$1.250.00, calcule um aumento 10%.
# Para os inferiores ou iguais, o aumento é de 15%.

salar = float(input('Qual o valor do salário: '))
if salar >= 1250:
    print('O salário R${:.2f}, terá um aumento de {:.2f}.'.format(salar,salar + (salar*10/100)))
else:
    print('O salário R${:.2f}, terá um aumento de {:.2f}.'.format(salar,salar + (salar*15/100)))