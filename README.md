<h1>Apredendo python</h1>

Documentação dos projetos em python criados e, primeiros commits de aulas e exercícios.

Criei um diretório especifico para as aulas e para os exercícios.
A maior parte das lições e observações aprendidas estão comentadas
nos códigos.

A ideia geral desse repositório é juntar todas as informações e ensinamentos que foram
explorados ao decorrer do curso. 
